"""Firestore target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th
from typing import Callable, Dict, List, Optional, Tuple, Type, Union
from pathlib import Path, PurePath
import collections
import firebase_admin
from firebase_admin import credentials

from target_firestore.sinks import (
    FirestoreSink,
)


class TargetFirestore(Target):
    """Sample target for Firestore."""
    def __init__(
        self,
        config: Optional[Union[dict, PurePath, str, List[Union[PurePath, str]]]] = None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        """Initialize the target.

        Args:
            config: Target configuration. Can be a dictionary, a single path to a
                configuration file, or a list of paths to multiple configuration
                files.
            parse_env_config: Whether to look for configuration values in environment
                variables.
            validate_config: True to require validation of config settings.
        """
        super().__init__(config, parse_env_config, validate_config)
        cred = credentials.Certificate(dict(self._config))
        firebase_admin.initialize_app(cred)

    name = "target-firestore"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "private_key_id",
            th.StringType,
            required=True,
            description="Private key Id for freibase"
        ),
        th.Property(
            "project_id",
            th.StringType,
            required=True,
            description="Project IDs"
        ),
        th.Property(
            "private_key",
            th.StringType,
            description="Private key for firebase."
        ),
        th.Property(
            "auth_uri",
            th.StringType,
            default="https://accounts.google.com/o/oauth2/auth",
            description="The url for the API service"
        ),
        th.Property(
            "token_uri",
            th.StringType,
            default="https://oauth2.googleapis.com/token",
            description="The url for the API service"
        ),
        th.Property(
            "auth_provider_x509_cert_url",
            th.StringType,
            default="https://www.googleapis.com/oauth2/v1/certs",
            description="The url for the API service"
        ),
        th.Property(
            "client_x509_cert_url",
            th.StringType,
            default="https://www.googleapis.com/robot/v1/metadata/x509/tap-firestore%40hotglue.iam.gserviceaccount.com",
            description="Certificate Url"
        ),
    ).to_dict()
    default_sink_class = FirestoreSink

if __name__ == '__main__':
    TargetFirestore.cli()            
